var strPl1 = [],
    strPl2 = [],
    btnsRow = [],
    btnsCol = [],
    generationBtn = document.getElementById('generation'),
    blockPlayer = document.getElementById('playerTable'),
    blockPlayer1 = document.getElementById('playerTable1');
    //blockResult = document.getElementById('resultTable')

function randomInteger() {
    var min = document.getElementById('min').value;
    var max = document.getElementById('max').value;
    var rand = min - 0.5 + Math.random() * (max - min + 1);
    rand = Math.round(rand);
    return rand;
}

function createArray(rows, columns) {
    var arr = [];
    for(var i=0; i<rows; i++){
        arr[i] = [];
        for(var j=0; j<columns; j++){
            arr[i][j] = randomInteger();
        }
    }
    return arr;
}

function createTable(el,str1, str2) {
    var block = document.createElement('div');
    var tableObj = document.createElement('table');
    el.appendChild(block);

    block.appendChild(tableObj);
    for (var i = 0; i < str1.length; ++i) {
        var tr = document.createElement('tr');
        for (var j = 0; j < str2.length; ++j) {
            var td = document.createElement('td');
            var input = document.createElement('input');
            input.style.width='40px';
            input.style.border='none';
            td.appendChild(input);
            input.value = str1[i][j]+' ; ' +str2[j][i];
            tr.appendChild(td);
        }
        tableObj.appendChild(tr);
    }
    return tableObj;
}

function createDelBtns(rows, cells, table) {
   var tr = document.createElement('tr');
    table.appendChild(tr);
    for (var i = 0; i<rows;i++){
        var td1 = document.createElement('td');
        tr.appendChild(td1);
        var btnRow = document.createElement('button');
        btnsRow[i]=btnRow;
        btnRow.addEventListener('click', function(){del(btnsRow.indexOf(this), btnsRow ,table);});
        btnRow.innerHTML='X';
        btnRow.className='del';
        td1.appendChild(btnRow);
    }

    for(var j = 0; j<cells; j++){
        var btnCol = document.createElement('button');
        var td = document.createElement('td');
        btnsCol[j]=btnCol;
        btnCol.addEventListener('click', function(){del(btnsCol.indexOf(this), btnsCol, table);});
        btnCol.innerHTML='X';
        btnCol.className='del';
        td.appendChild(btnCol);
        table.getElementsByTagName('tr')[j].appendChild(td);
    }
}

function showTable() {
    generationBtn.disabled=true;
    var rows = document.getElementById('strat1').value;
    var cells = document.getElementById('strat2').value;
    strPl1 = createArray( cells, rows);
    strPl2 = createArray( rows, cells);
    var table =  createTable(blockPlayer, strPl1, strPl2);
    createDelBtns(rows, cells, table);
    var table1 =  createTable(blockPlayer1, strPl1, strPl2);
    var arrInpt = table1.getElementsByTagName('input');

    for(var i = 0; i< arrInpt.length; i++){
        arrInpt[i].addEventListener('click', function () {

                this.style.background = 'green';
        });
        arrInpt[i].addEventListener('dblclick', function () {
            this.style.background = 'orange';
        })
    }
}


function del(index, arr, table) {
    if(arr ===btnsCol&&btnsCol.length>1){
        btnsCol.splice(index, 1);
        table.removeChild(table.getElementsByTagName('tr')[index]);
    }

    if(arr===btnsRow&&btnsRow.length>1) {
        var tr = table.getElementsByTagName('tr');
        btnsRow.splice(index, 1);
        for (var i = 0; i < tr.length; i++) {
            var td = table.getElementsByTagName('tr')[i].getElementsByTagName('td')[index];
            td.parentNode.removeChild(td);
        }
    }
}

generationBtn.addEventListener('click', showTable);
